package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"bd2l.com.au/csmu/utilities"
)

type fileInputOutput struct {
	Path   *string `json:"path"`
	Exists bool    `json:"exists"`
}

type inputOutput struct {
	Input  fileInputOutput `json:"input"`
	Output fileInputOutput `json:"output"`
}

type version struct {
	Prefix  *string   `json:"prefix"`
	Major   *int      `json:"major"`
	Minor   *int      `json:"minor"`
	Commits *int      `json:"commits"`
	Bug     time.Time `json:"bug"`
	Text    string    `json:"text"`
}

func (version *version) mapPrevious(previous *version, incrementMajor *bool, incrementMinor *bool) {
	if version != nil {
		if previous != nil {

			var prefix = *version.Prefix
			var major = *version.Major
			var minor = *version.Minor
			var commits = *version.Commits
			var location *time.Location
			var dash = ""
			location, _ = time.LoadLocation("UTC")

			if prefix == "" {
				version.Prefix = previous.Prefix
			}
			if major == -1 {
				major = *previous.Major
				if *incrementMajor == true {
					major++
				}
				version.Major = &major
			}
			if minor == -1 {
				minor = *previous.Minor
				if *incrementMinor == true {
					minor++
				}
				version.Minor = &minor
			}
			if commits == -1 {
				commits = *previous.Commits
			}
			commits = commits + 1
			version.Commits = &commits
			if *version.Prefix != "" {
				dash = "-"
			}
			version.Text = fmt.Sprintf("%s%sv%d.%d.%d.%s", *version.Prefix, dash, *version.Major, *version.Minor, *version.Commits, version.Bug.In(location).Format("20060102-150405-UTC"))
		}
	}
}

func loadFrom(path string) (*version, error) {

	var err error
	var byteSlice []byte
	var file *os.File
	var result *version

	file, err = os.Open(path)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		byteSlice, err = ioutil.ReadAll(file)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			err = json.Unmarshal(byteSlice, &result)
			if err != nil {
				fmt.Println(err.Error())
			}
		}
	}

	return result, err
}

func main() {

	var byteSlice []byte
	var directoryPath string
	var err error
	var file *os.File
	var inputOutput = &inputOutput{}
	var previousVersion = &version{}
	var versionPath string
	var version = &version{}
	var incrementMajor *bool
	var incrementMinor *bool
	var versionCodeTemplateFile *os.File
	var versionCodeTemplatePath *string
	var versionCodeTemplateByteSlice []byte
	var versionCodeFile *os.File
	var versionCodePath *string
	var versionCodeByteSlice []byte

	directoryPath, err = filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	versionPath = filepath.Join(directoryPath, "version.json")

	inputOutput.Input.Path = flag.String("input", "", "A path to a valid json version input file. Default is version.json")
	inputOutput.Output.Path = flag.String("output", "", "A path to a valid json version output file. Default is version.json")

	if *inputOutput.Input.Path == "" {
		inputOutput.Input.Path = &versionPath
	}
	if *inputOutput.Output.Path == "" {
		inputOutput.Output.Path = &versionPath
	}
	inputOutput.Input.Exists = utilities.PathExists(*inputOutput.Input.Path)
	inputOutput.Output.Exists = utilities.PathExists(*inputOutput.Output.Path)

	version.Prefix = flag.String("prefix", "", "The prefix you'd like for the project")
	version.Major = flag.Int("major", -1, "The major version of the project")
	version.Minor = flag.Int("minor", -1, "The minor version of the project")
	version.Commits = flag.Int("commits", -1, "The number of commits in the project")
	version.Bug = time.Now()

	incrementMajor = flag.Bool("increment-major", false, "Pass to increment the major version")
	incrementMinor = flag.Bool("increment-minor", false, "Pass to increment the minor version")

	versionCodeTemplatePath = flag.String("version-code-template-path", "./version.go.template", "The path to the version code template")
	versionCodePath = flag.String("version-code-path", "../version.go", "The path to the version code")

	flag.Parse()

	fmt.Printf("increment-major: %v\n", *incrementMajor)
	fmt.Printf("increment-minor: %v\n", *incrementMinor)

	byteSlice, err = json.Marshal(inputOutput)
	if err != nil {
		log.Fatal(err)
	} else {
		if inputOutput.Input.Exists {
			previousVersion, err = loadFrom(*inputOutput.Input.Path)
			if err != nil {
				fmt.Println(err.Error())
			} else {
				version.mapPrevious(previousVersion, incrementMajor, incrementMinor)
				byteSlice, err = json.MarshalIndent(version, "", "    ")
				fmt.Printf("%s\n", string(byteSlice))
				file, err = os.Create(*inputOutput.Output.Path)
				if err != nil {
					fmt.Println(err.Error())
				} else {
					defer file.Close()
					file.WriteString(string(byteSlice))
					file.Sync()
					if *versionCodeTemplatePath != "" {
						versionCodeTemplateFile, err = os.Open(*versionCodeTemplatePath)
						if err != nil {
							fmt.Println(err.Error())
						} else {
							defer versionCodeTemplateFile.Close()
							versionCodeTemplateByteSlice, err = ioutil.ReadAll(versionCodeTemplateFile)
							if err != nil {
								fmt.Println(err.Error())
							} else {
								versionCodeByteSlice = bytes.ReplaceAll(versionCodeTemplateByteSlice, []byte(`{{version.json}}`), byteSlice)
								versionCodeFile, err = os.Create(*versionCodePath)
								if err != nil {
									fmt.Println(err.Error())
								} else {
									defer versionCodeFile.Close()
									versionCodeFile.Write(versionCodeByteSlice)
								}
							}
						}
					}
				}
			}
		}
	}
}
