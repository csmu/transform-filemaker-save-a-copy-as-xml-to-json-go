# Go parameters

# https://sohlich.github.io/post/go_makefile/

GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=transform-filemaker-save-a-copy-as-xml-to-json

all: build
build: 
	$(GOBUILD) -o $(BINARY_NAME) -v
	
install: build
	sudo cp transform-filemaker-save-a-copy-as-xml-to-json /usr/local/bin
	
uninstall:
	sudo rm /usr/local/bin/transform-filemaker-save-a-copy-as-xml-to-json
	
install-pre-commit-macosx:
	cp pre-commit-macosx .git/hooks/pre-commit

uninstall-pre-commit:
	rm .git/hooks/pre-commit
	
