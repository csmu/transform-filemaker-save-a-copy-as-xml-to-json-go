package main

import (
	"github.com/Jeffail/gabs"
)

func Version() string {
	var result string
	var err error
	var gabsContainer *gabs.Container
	gabsContainer, err = gabs.ParseJSON([]byte(`
	{
    "prefix": "transform-filemaker-save-a-copy-as-xml-to-json",
    "major": 1,
    "minor": 1,
    "commits": 37,
    "bug": "2021-05-13T17:40:15.178105+09:30",
    "text": "transform-filemaker-save-a-copy-as-xml-to-json-v1.1.37.20210513-081015-UTC"
}
	`))
	if err != nil {
		result = err.Error()
	} else {
		result = gabsContainer.StringIndent("", "    ")
	}
	return result
}
