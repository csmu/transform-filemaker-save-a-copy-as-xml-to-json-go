package main

import (
	"bytes"
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"bd2l.com.au/csmu/filemaker"
	"bd2l.com.au/csmu/utilities"
	"github.com/joho/godotenv"
	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/unicode"
	"golang.org/x/text/transform"
)

func main() {

	var applicationDirectory string
	var environmentPath string
	var fmDynamicTemplate *filemaker.FMDynamicTemplate
	var byteSlice []byte
	var err error
	var directoryPath = "./"
	var pathSlice []string
	var path string
	var transformReader *transform.Reader
	var xmlDecoder *xml.Decoder
	var last time.Time
	var elapsed time.Duration
	var versionFlag *bool

	versionFlag = flag.Bool("version", false, "Output the versin information")
	flag.Parse()

	if *versionFlag == true {
		fmt.Println(Version())
	} else {

		last = time.Now()

		applicationDirectory, err = filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			log.Fatal(err)
		}
		environmentPath = filepath.Join(applicationDirectory, ".env")
		if utilities.PathIsFile(environmentPath) {
			if utilities.PathExists(environmentPath) {
				fmt.Println(environmentPath)
				err = godotenv.Load(environmentPath)
			}
		}
		if err != nil {
			panic(err)
		}

		if len(os.Args) > 1 {
			directoryPath = os.Args[1]
		} else {
			for _, e := range os.Environ() {
				pair := strings.Split(e, "=")
				switch pair[0] {
				case "FILEMAKER_DEFAULT_PROJECT_ROOT":
					directoryPath = pair[1]
				}
			}
		}

		fmt.Printf("directoryPath: %s\n", directoryPath)
		pathSlice, err = readConfig(directoryPath)

		if err != nil {
			panic(err)
		} else {
			for _, path = range pathSlice {

				transformReader, err = getTransformReader(path)
				byteSlice, err = ioutil.ReadAll(transformReader)

				if err != nil {
					panic(err)
				} else {
					nulledByteSlice := bytes.ReplaceAll(byteSlice, []byte("\x00"), []byte("null"))
					fmDynamicTemplate = &filemaker.FMDynamicTemplate{}
					xmlDecoder = xml.NewDecoder(bytes.NewReader(nulledByteSlice))
					err = xmlDecoder.Decode(&fmDynamicTemplate)
					if err != nil {
						panic(err)
					} else {
						fmt.Printf("%+v\n", fmDynamicTemplate.File)
						err = fmDynamicTemplate.Process(path)
						elapsed = time.Since(last)
						fmt.Printf("took %s\n", elapsed)
						last = time.Now()
						if err != nil {
							fmt.Println(err)
						}
					}
				}
			}
			fmt.Println(Version())
		}
	}
}

func createBaseDirectoryCatalogDirectories(baseDirectoryCatalog *filemaker.BaseDirectoryCatalog, rootPath string) error {
	var err error
	var directoryPath string
	var metaPath string
	var baseDirectory *filemaker.BaseDirectory
	if baseDirectoryCatalog != nil {
		for _, baseDirectory = range baseDirectoryCatalog.BaseDirectorySlice {
			directoryPath = filepath.Join(rootPath, baseDirectory.Name)
			err = createDirectoryIfItDoesNotExist(directoryPath, 0775)
			if err == nil {
				metaPath = ".meta.json"
				err = utilities.SaveJSONByteSliceToPath(baseDirectory, directoryPath, metaPath)
				if err != nil {
					break
				}
			}
		}
	}
	return err
}

func createBaseTableDirectories(baseTableCatalog *filemaker.BaseTableCatalog, rootPath string) error {
	var err error
	var directoryPath string
	var metaPath string
	var baseTable *filemaker.BaseTable
	if baseTableCatalog != nil {
		for _, baseTable = range baseTableCatalog.BaseTableSlice {
			directoryPath = filepath.Join(rootPath, baseTable.Name)
			err = createDirectoryIfItDoesNotExist(directoryPath, 0775)
			if err == nil {
				metaPath = ".meta.json"
				err = utilities.SaveJSONByteSliceToPath(baseTable, directoryPath, metaPath)
				if err != nil {
					break
				}
			}
		}
	}
	return err
}

func createTableOccurencesDirectories(tableOccurrenceCatalog *filemaker.TableOccurrenceCatalog, rootPath string) error {
	var err error
	var directoryPath string
	var metaPath string
	var tableOccurrence *filemaker.TableOccurrence
	if tableOccurrenceCatalog != nil {
		for _, tableOccurrence = range tableOccurrenceCatalog.TableOccurrenceSlice {
			directoryPath = filepath.Join(rootPath, tableOccurrence.Name)
			err = createDirectoryIfItDoesNotExist(directoryPath, 0775)
			if err == nil {
				metaPath = ".meta.json"
				err = utilities.SaveJSONByteSliceToPath(tableOccurrence, directoryPath, metaPath)
				if err != nil {
					break
				}
			}
		}
	}
	return err
}

// CreateDirectoryIfItDoesNotExist ...
// https://siongui.github.io/2017/03/28/go-create-directory-if-not-exist/
func createDirectoryIfItDoesNotExist(path string, fileMode os.FileMode) error {
	var err error
	if _, err = os.Stat(path); os.IsNotExist(err) {
		err = os.MkdirAll(path, fileMode)
	}
	return err
}

func getTransformReader(path string) (*transform.Reader, error) {

	var err error
	var file *os.File
	var utf16littleEndian encoding.Encoding
	var utf16ByteOrderMarker transform.Transformer
	var transformReader *transform.Reader

	file, err = os.Open(path)
	if err == nil {
		utf16littleEndian = unicode.UTF16(unicode.LittleEndian, unicode.IgnoreBOM)
		utf16ByteOrderMarker = unicode.BOMOverride(utf16littleEndian.NewDecoder())
		transformReader = transform.NewReader(file, utf16ByteOrderMarker)
	}

	return transformReader, err
}

func readConfig(dirctoryPath string) ([]string, error) {

	var err error
	var result []string
	var name string
	var fileInfoSlice []os.FileInfo
	var fileInfo os.FileInfo

	fileInfoSlice, err = ioutil.ReadDir(dirctoryPath)
	if err == nil {
		for _, fileInfo = range fileInfoSlice {
			name = fileInfo.Name()
			if path.Ext(name) == ".xml" {
				result = append(result, filepath.Join(dirctoryPath, fileInfo.Name()))
			}
		}
	}

	return result, err
}

type version struct {
	Prefix  *string   `json:"prefix"`
	Major   *int      `json:"major"`
	Minor   *int      `json:"minor"`
	Commits *int      `json:"commits"`
	Bug     time.Time `json:"bug"`
	Text    string    `json:"text"`
}
