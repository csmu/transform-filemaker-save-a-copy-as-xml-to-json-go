# filemaker-xml-source-to-json

## Example usage

```

./transform-filemaker-save-a-copy-as-xml-to-json # from the directory containing the filemaker-save-as-aml files

./transform-filemaker-save-a-copy-as-xml-to-json {path-to-directory-of-filemaker-save-as-xml-files}

```
